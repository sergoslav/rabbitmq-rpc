<?php

return [

    "rabbitmq" => [
        "host" => env("SLV_RABBITMQ_RPC_HOST"),
        "port" => env("SLV_RABBITMQ_RPC_PORT"),
        "username" => env("SLV_RABBITMQ_RPC_USER"),
        "password" => env("SLV_RABBITMQ_RPC_PASSWORD"),

        "modules" => [
            "rabbitmq_delayed_message_exchange" => env("SLV_RABBITMQ_RPC_MODULE_DELAYED_MESSAGE_EXCHANGE", false)
        ]
    ],

    "exchange" => env('SLV_RABBITMQ_RPC_EXCHANGE', 'rpc-exchange'),
    "queue" => [
        "prefix" => env('SLV_RABBITMQ_RPC_QUEUE_PREFIX', 'slv-rpc'),
    ],

    "services" => [
        env('SLV_RABBITMQ_RPC_EXAMPLE_SERVICE_CODE') => [
            "key" => env('SLV_RABBITMQ_RPC_EXAMPLE_SERVICE_KEY')
        ]
    ]
];
