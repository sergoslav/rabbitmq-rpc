<?php

namespace Sergoslav\RabbitMqRpc;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Queue
{
    protected AMQPStreamConnection $connection;

    protected static ?AMQPChannel $channel = null;
    protected string $exchangeName;
    protected bool $delayAvailable = false;

    public function __construct()
    {
        $this->exchangeName = config("rabbitmq-rpc.exchange");
        $this->delayAvailable = (bool)config("rabbitmq-rpc.rabbitmq.modules.rabbitmq_delayed_message_exchange");

        if (!self::$channel) {
            #init RabbitMq
            $this->connection = new AMQPStreamConnection(
                config("rabbitmq-rpc.rabbitmq.host"),
                config("rabbitmq-rpc.rabbitmq.port"),
                config("rabbitmq-rpc.rabbitmq.username"),
                config("rabbitmq-rpc.rabbitmq.password")
            );

            self::$channel = $this->connection->channel();

            $exchangeType = 'direct';
            $exchangeArguments = [];

            if ($this->isDelayAvailable()) {
                $exchangeType = 'x-delayed-message';
                $exchangeArguments['x-delayed-type'] = ['S', 'direct'];
            }

            self::$channel->exchange_declare(
                $this->exchangeName,
                $exchangeType,
                false,
                true,
                false,
                false,
                false,
                $exchangeArguments
            );

            #init queues
            foreach (config("rabbitmq-rpc.services") as $serviceCode => $serviceData) {
                foreach ([true, false] as $isAsync) {
                    $queueName = self::getQueueName($serviceCode, $isAsync);
                    $routingKey = self::getRoutingKey($serviceCode, $isAsync);
                    if (self::$channel->queue_declare($queueName, false, true, false, false)) {
                        self::$channel->queue_bind($queueName, $this->exchangeName, $routingKey);
                    }
                }

            }
        }
    }

//    public function __destruct()
//    {
//        foreach (self::$channels as $channel) {
//            $channel->close();
//        }
//        dump(__METHOD__.__LINE__);
//        $this->connection->close();
//    }

    public function getChannel(): AMQPChannel
    {
        return self::$channel;
    }

    public function getExchangeName(): string
    {
        return $this->exchangeName;
    }

    public function isDelayAvailable(): bool
    {
        return $this->delayAvailable;
    }

    public static function getQueueName(string $serviceCode, bool $async = false): string
    {
        $serviceKey = config("rabbitmq-rpc.services.{$serviceCode}.key");
        $postfix = $async ? "async" : "sync";
        $prefix = config("rabbitmq-rpc.queue.prefix");
        return "{$prefix}.service-{$serviceCode}-{$postfix}-.". md5($serviceKey);
    }

    public static function getRoutingKey(string $serviceCode, bool $async = false): string
    {
        $serviceKey = config("rabbitmq-rpc.services.{$serviceCode}.key");
        $postfix = $async ? "async" : "sync";
        return "service.{$serviceCode}-{$postfix}-" . md5($serviceKey);
    }

}
