<?php

namespace Sergoslav\RabbitMqRpc\RpcServer;

use Sergoslav\RabbitMqRpc\RpcCallResponse;

interface CallHandler
{
    public function isMethodExists(string $method): bool;
    public function validateCallParameters(string $method, array $args): array;
    public function call(string $method, array $args): RpcCallResponse;
    public function callAsync(string $method, array $args): bool;
    public function finish();
}
