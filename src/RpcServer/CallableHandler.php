<?php

namespace Sergoslav\RabbitMqRpc\RpcServer;

use ReflectionMethod;
use ReflectionParameter;
use Sergoslav\RabbitMqRpc\RpcCallResponse;

trait CallableHandler
{
    private static array $functionParameters = [];

    public function isMethodExists(string $method): bool
    {
        return method_exists($this, $method);
    }

    public function validateCallParameters(string $method, array $args): array
    {
        try {
            if (!isset(self::$functionParameters[$method])) {
                $r = new ReflectionMethod($this, $method);
                self::$functionParameters[$method] = $r->getParameters();
            }
        } catch (\ReflectionException) {
            return ["validateCallParameters ReflectionException"];
        }

        $troubles = [];
        /** @var ReflectionParameter $param */
        foreach (self::$functionParameters[$method] as $parameter) {
            if (!$parameter->isOptional() && !isset($args[$parameter->name])) {
                $troubles[] = "Parameter {$parameter->name} required and absent";
            } elseif (!in_array(get_debug_type($args[$parameter->name]), explode("|", $parameter->getType()))) {
                $troubles[] = "Parameter {$parameter->name} has ". gettype($args[$parameter->name]) ." type. Expected {$parameter->getType()}";
            }
        }

        return $troubles;
    }

    public function call(string $method, array $args): RpcCallResponse
    {
        if ($troubles = $this->validateCallParameters($method, $args)) {
            return RpcCallResponse::initialize(['troubles' => $troubles], false, RpcCallResponse::STATUS_FUNCTION_PARAMETERS_ERROR);
        }

        try {
            return call_user_func_array([$this, $method], $args);
        } catch (\Throwable $e) {
            return RpcCallResponse::initialize(['error' => $e->getMessage(), 'code' => $e->getCode()], false, RpcCallResponse::STATUS_FUNCTION_CALL_ERROR);
        }
    }

    public function callAsync(string $method, array $args): bool
    {
        if (!$this->validateCallParameters($method, $args)) {
            try {
                call_user_func_array([$this, $method], $args);

                return true;
            } catch (\Throwable $e) {
                #TODO: log error
            }
        }

        return  false;
    }

    public function finish()
    {
        #Do some job after queue worker stopped
    }
}
