<?php

namespace Sergoslav\RabbitMqRpc;

use Illuminate\Support\Arr;
use JetBrains\PhpStorm\Pure;

class RpcCallResponse
{
    const PARAM_SUCCESS = 'success';
    const PARAM_STATUS = 'status';
    const PARAM_RESULT = 'result';

    const STATUS_SUCCESS = 200;
    const STATUS_NOT_FOUND = 404;
    const STATUS_FUNCTION_NOT_FOUND = 405;
    const STATUS_FUNCTION_PARAMETERS_ERROR = 400;
    const STATUS_FUNCTION_CALL_ERROR = 500;

    public function __construct(
        public mixed $result,
        public bool $success,
        public int $status
    ) {

    }

    #[Pure] public static function initialize(mixed $result, bool $success = true, int $status = self::STATUS_SUCCESS): self
    {
        return new self($result, $success, $status);
    }

    public static function initializeFromResponse(string $responseBody): self
    {
        $data = json_decode($responseBody, true);
        return new self($data[self::PARAM_RESULT], $data[self::PARAM_SUCCESS], $data[self::PARAM_STATUS]);
    }

    public function __toString(): string
    {
        return $this->getBody();
    }

    public function getBody():string
    {
        return json_encode([
            self::PARAM_SUCCESS => $this->success,
            self::PARAM_STATUS => $this->status,
            self::PARAM_RESULT => $this->result,
        ]);
    }

    public function isSuccess(): bool
    {
        return $this->success;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getResult(string $key = ""): mixed
    {
        if ($key && is_array($this->result)) {
            return Arr::get($this->result, $key);
        } elseif ($key) {
            return null;
        }
        return $this->result;
    }


}
