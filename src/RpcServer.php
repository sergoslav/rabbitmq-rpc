<?php

namespace Sergoslav\RabbitMqRpc;

use PhpAmqpLib\Message\AMQPMessage;
use Sergoslav\RabbitMqRpc\RpcServer\CallHandler;

class RpcServer
{
    const SERVER_TYPE_ASYNC = 'async';
    const SERVER_TYPE_SYNC = 'sync';

    private Queue $queue;
    private $stopConsume = false;

    public function __construct() {
        $this->queue = app()->make(Queue::class);

        pcntl_signal(SIGINT, [$this, 'stopConsume']);
    }

    public function stopConsume()
    {
        $this->stopConsume = true;
    }

    public function run(string $service, CallHandler $handler, string $serverType)
    {
        echo " [*] Waiting for logs. To exit press CTRL+C\n";
        $isAsync = ($serverType == self::SERVER_TYPE_ASYNC);

        $this->queue->getChannel()->basic_qos(null, 1, null);
        $this->queue->getChannel()->basic_consume(Queue::getQueueName($service, $isAsync), '', false, false, false, false, $this->getCallbackFunction($handler));
        while ($this->queue->getChannel()->is_open() && $this->stopConsume !== true) {
            $this->queue->getChannel()->wait();
        }

        $handler->finish();
    }

    protected function getCallbackFunction(CallHandler $handler): callable
    {
        return function (AMQPMessage $req) use($handler) {
            $rpcRequest = RpcCallRequest::initializeFromRequest($req->getBody());

            if ($req->has('reply_to')) {
                if ($handler->isMethodExists($rpcRequest->getMethod())) {
                    $response = $handler->call($rpcRequest->getMethod(), $rpcRequest->getArgs());
                } else {
                    $response = RpcCallResponse::initialize(null, false, RpcCallResponse::STATUS_FUNCTION_NOT_FOUND);
                }

                $msg = new AMQPMessage(
                    $response->getBody(),
                    array('correlation_id' => $req->get('correlation_id'))
                );

                $req->getChannel()->basic_publish(
                    $msg,
                    '',
                    $req->get('reply_to')
                );
                $req->ack();
            } else {
                if ($handler->isMethodExists($rpcRequest->getMethod())) {
                    $result = $handler->callAsync($rpcRequest->getMethod(), $rpcRequest->getArgs());
                } else {
                    #TODO: error
                }
                $req->ack();
            }
        };
    }
}
