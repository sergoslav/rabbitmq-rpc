<?php

namespace Sergoslav\RabbitMqRpc;

use JetBrains\PhpStorm\Pure;

class RpcCallRequest
{
    const PARAM_METHOD = 'm';
    const PARAM_ARGS = 'a';

    public function __construct(
        public string $method,
        public array $args
    ) {

    }

    #[Pure] public static function initialize(string $method, array $args): self
    {
        return new self($method, $args);
    }

    public static function initializeFromRequest(string $requestBody): self
    {
        $data = json_decode($requestBody, true);
        return new self($data[self::PARAM_METHOD], $data[self::PARAM_ARGS]);
    }

    public function __toString(): string
    {
        return $this->getBody();
    }

    public function getBody():string
    {
        return json_encode([
            self::PARAM_METHOD => $this->method,
            self::PARAM_ARGS => $this->args,
        ]);
    }

    public function getHash(): string
    {
        return md5($this->method . serialize($this->args));
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getArgs(): array
    {
        return $this->args;
    }
}
