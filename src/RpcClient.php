<?php

namespace Sergoslav\RabbitMqRpc;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;

class RpcClient
{
    private Queue $queue;
    private AMQPChannel $channel;
    private string $callback_queue;
    private ?string $response;
    private string $corr_id;
    private static ?RpcClient $instance = null;

    public static function getInstance(): self
    {
        if (!self::$instance) {
            self::$instance = app()->make(self::class);
        }

        return self::$instance;
    }

    public function __construct() {
        $this->queue = app()->make(Queue::class);
        $this->channel = $this->queue->getChannel();

        [$this->callback_queue, ,] = $this->channel->queue_declare(
            "",
            false,
            false,
            true,
            false
        );

        $this->channel->basic_consume(
            $this->callback_queue,
            '',
            false,
            true,
            false,
            false,
            array(
                $this,
                'onResponse'
            )
        );
    }

    public function onResponse($rep)
    {
        if ($rep->get('correlation_id') == $this->corr_id) {
            $this->response = $rep->body;
        } else {
//            dump("correlation_id doens't match");
        }
    }

    public function call(string $service, string $method, array $args = [], int $delay = 0): RpcCallResponse
    {
        $request = rpcCallRequest::initialize($method, $args);

        $this->response = null;
        $this->corr_id = uniqid($request->getHash());

        $msg = new AMQPMessage(
            $request->getBody(),
            [
                'correlation_id' => $this->corr_id,
                'reply_to' => $this->callback_queue
            ]
        );

        $this->channel->basic_publish($msg, $this->queue->getExchangeName(), Queue::getRoutingKey($service));
        while (is_null($this->response)) {
            $this->channel->wait();
        }
        return RpcCallResponse::initializeFromResponse($this->response);
    }

    /**
     * @param string $service
     * @param string $method
     * @param array $args
     * @param int $delay # delay in milliseconds = 1/1000 sec
     */
    public function callAsync(string $service, string $method, array $args = [], int $delay = 0)
    {
        $request = rpcCallRequest::initialize($method, $args);
        $metadata = [
            'application_headers' => new \PhpAmqpLib\Wire\AMQPTable([
                'x-delay' => $delay,
            ]),
        ];

        $msg = new AMQPMessage($request->getBody(), $metadata);
        $this->channel->basic_publish($msg, $this->queue->getExchangeName(), Queue::getRoutingKey($service, true));
    }

    public function isDelayedCallAvailable(): bool
    {
        return $this->queue->isDelayAvailable();
    }
}
